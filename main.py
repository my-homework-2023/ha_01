import asyncio
import json
import aiohttp
import re
import datetime

OPENWEATHER_API_KEY = '0fb610dab7456bc44dbdde2ddba9be71'

class CityWeather:
    def __init__(self, city, temperature, humidity, description):
        self.city = city
        self.temperature = temperature
        self.humidity = humidity
        self.description = description

async def fetch_weather(session, city):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={OPENWEATHER_API_KEY}"
    async with session.get(url) as response:
        data = await response.json()
        temperature = data["main"]["temp"]
        humidity = data["main"]["humidity"]
        description = data["weather"][0]["description"]
        return CityWeather(city, temperature, humidity, description)

async def get_weather_for_cities(cities):
    async with aiohttp.ClientSession() as session:
        tasks = []
        for city in cities:
            task = asyncio.create_task(fetch_weather(session, city))
            tasks.append(task)
        weather_data = await asyncio.gather(*tasks)
        return weather_data

async def read_cities_from_file(file_path):
    cities = []
    with open('cities.txt', 'r') as file:
        lines = file.readlines()
        for line in lines:
            city = re.findall(r'[A-z]+',line)
            if len(city) > 1:
                cities.append(city[0]+' '+city[1])
            elif len(city) == 1:
                cities.append(city[0])
    return cities

async def main():
    cities = await read_cities_from_file("cities.txt")
    weather_data = await get_weather_for_cities(cities)
    for weather in weather_data:
        print(f"Weather in {weather.city}:")
        print(f"Temperature: {weather.temperature} K")
        print(f"Humidity: {weather.humidity}%")
        print(f"Description: {weather.description}")
        print()

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    asyncio.run(main())
    
    print('time run', datetime.datetime.now()-start_time, 'sec')